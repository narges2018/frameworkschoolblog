//------------------ config ------------------------------
app.name = 'dev';

app.cloadio = {
	// you can enable or disable all functions related to cloadio.
	// if you don't need back end disable it.
	enabled: true,

	// you can execute code on all devices remotely.
	allowRemoteExecution: true,

	// public users info path
	puip: '/users'
};

app.version = {
	// control min version
	// if minimum version be greater than the current version update page will be shown
	// so app must have update page
	control: false,

	// current version
	current: 201705271523,

	// min version path
	mvp: '/update/minVersion',
};

app.update = {
	// auto update
	autoUpdate: true,

	// auto update url path
	auup: '/update/url'
};

// when authentication failed show login page automatically
app.auth.slpa = true;

// use device geolocation
app.location = {
	enabled: true
};

// use notification service
app.notification = {
	enabled: true
};

// android and ios status bar
app.statusBar = {
	color: '#000000',
	light: true
};

// android navigation bar
app.navigationBar = {
	color: '#000000',
	light: true
};
//------------------ end of config -----------------------

app.functions  = {};

app.functions.validateTel = function (input, callback) {
	input = tools.numsToLatin(input.trim());
	if (input.substring(0, 1) != '0' && input.substring(0, 1) != '+')
		input = '+' + input;
	if (/^(((00|\+)[1-9]{1,3})|0)[1-9]\d{9}$/.test(input)) {
		if (input.substring(0, 2) === '00')
			input = input.substring(2);
		else if (input.substring(0, 1) === '+')
			input = input.substring(1);
		else
			input = 98 + input.substring(1);
		callback && callback(null, input);
	}
	else if (callback)
		callback('invalid tel number');
};

// ----------------- events ------------------------------
// on document ready event
// called when html dom content loaded
app.onDocumentReady = function () {
	// loginPage.show();
	// verifyPage.show();
	// userInfoPage.show();
	// homePage.show();
	// userFavorites.show();
	// profilePage.show();	
	// blogPage.show();
};

// on connection open event
app.onOpen = function () {
	// body...
};

// on connection close event
app.onClose = function () {
	// body...
};

// on connection open for first time event
app.onConnect = function (error, credential) {
	// body...
};

// on sync event
// called when time synced with cloadio
app.onSync = function () {
	// body...
	// example: app.now()
};

// called when user object synced with cloadio
app.auth.onCompleted = function () {
	newPostPage.show();
};

app.beforePageChanged = function (name) {
	// body...
};

app.afterPageChanged = function (name) {
	// body...
};

// on back button event just in android
app.control.onBack = function () {
	// body...
	Android.exit();
};

// on location changed event
app.location.onChanged = function (longitude, latitude) {
	// body...
};

// on notification received event
app.notification.onReceived = function (data) {
	// body...
};

// on sms received event
app.auth.sms.onReceived = function (text, sender) {
	// body...
};

// on update started
app.update.onStarted = function () {
	// body...
};

// on update progress changed
app.update.onProgressChanged = function (percentage) {
	// body...
};

// on update started
app.update.onCompleted = function () {
	// body...
};

// on std out event
// called when data is written to console
app.debug.onSTDOut = function (data) {
	// body...
};
// ----------------- end of events -----------------------