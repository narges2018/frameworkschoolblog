var newPostPage = new Page('newPost', function () {
    // after page shown

    $('.page.newPost > .content > .save').click(function () {
        var post = {
            createdAt: app.now(),
            createdBy: app.auth.user.id,
            title: $('.page.newPost > .content > .title > input').val(),
            body: $('.page.newPost > .content > .body > textarea').val()
        };

        // validation
        if (post.title === '')
            alert('عنوان الزامی است.');
        else
            ref.child('posts').push(post, function (error, postKey) {
                if (error)
                    alert('pushing post failed');
                else
                    alert('post pushed');
            });
    });
}, function () {
    // before page shown
});