var verifyPage = new Page('verify', function () {
    //after page shown

    // save authentication state
    localStorage['authentication'] = 'verifying';

    // define variables
    var jBtn = $('.page.verify > .content > .btn'),
    jInput = $('.page.verify > .content > input');

    jBtn.click(function () {
    	if (verifyPage.locked)
            return;
        verifyPage.locked = true;

        // prevent reconnection issue
        setTimeout(function () {
            verifyPage.locked = false;
        }, 20000);
        
        app.auth.sms.verify(tools.numsToLatin(jInput.val()), function (error, value) {
            verifyPage.locked = false;
            if (error) {
                console.error('verifying sms failed', error);
                app.controls.box('alert', null, 'کد مورد نظر صحیح نیست.', 'center');
            }
            else {
                localStorage['authentication'] = 'completed';
                app.auth.complete(function () {
                    // nothing
                });

                homePage.show();
                
                // on authentication completed event will be called automatically
            }
        });
    })

}, function () {
    //before page shown

});