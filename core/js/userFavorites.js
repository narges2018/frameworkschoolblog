const userFavoritesPage = new Page('userFavorites', function () {
	// after page shown
    let lorem = 'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی مورد نیاز، و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد، کتابهای زیادی در شصت و سه درصد گذشته حال و آینده، شناخت فراوان جامعه و متخصصان را می طلبد، تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی، و فرهنگ پیشرو در زبان فارسی ایجاد کرد، در این صورت می توان امید داشت که تمام و دشواری موجود در ارائه راهکارها، و شرایط سخت تایپ به پایان رسد و زمان مورد نیاز شامل حروفچینی دستاوردهای اصلی، و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد.';
    for (let i = 0; i < 9; i++) {
        let $post = $(
            '<div class="favoriteItem">' +
                '<div class="main">' +
                    '<img src="https://picsum.photos/id/'+i+'/500/500" alt="placeholder">' +
                    '<div class="mr-2 d-flex flex-column flex-1 h-100">' +
                        '<span class="mt-2">عنوان</span>' +
                        '<span class="body">'+truncate(lorem)+'</span>' +
                    '</div>' +
                '</div>'+
                '<div class="details">' +
                    '<span>نویسنده: ---</span>' +
                    '<span class="mr-5">دسته: ---</span>' +
                    '<span class="mr-5">تاریخ: ---</span>' +
                '</div>' +
            '</div>'
        );
        $post.click(() => alert('myItem: '+i+' Clicked'));
        $post.find('.favoriteBtn').click((e) => {
            e.stopPropagation();
            alert('myItem: '+i+' added to Favorites');
        });
        $('.page.userFavorites #favoriteList').append($post);
    }

    $('.page.userFavorites #btnBack').click(() => homePage.show());
}, function () {
	// before page shown
});