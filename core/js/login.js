var loginPage = new Page('login', function (force) {
    // after page shown

    // define variables
    var jBtn = $('.page.login > .content > .btn'),
    jInput = $('.page.login > .content > input');

    // force to show login page
    if (force) {
        // clean verifying state
        verifyPage.counter && verifyPage.counter.kill();
        localStorage.removeItem('authentication');
    }

    // switch to verify page
    if (localStorage['authentication'] === 'verifying') {
        verifyPage.show();
    }
    else {
        // restyle
        jBtn.html('ورود');
        jBtn.css('width', 60);
    }

    jBtn.click(function () {
        if (loginPage.locked)
            return;
        app.functions.validateTel(jInput.val(), function (error, value) {
            if (error) {
                app.controls.box('alert', null, 'شماره تلفن اشتباه است.', 'center');
                console.error('The phone number is not correct');
            }
            else if (value) {
                loginPage.locked = true;

                // restyle
                jBtn.html('در حال ارسال');
                jBtn.css('width', 100);

                // prevent reconnection issue
                setTimeout(function () {
                    loginPage.locked = false;
                }, 20000);

                app.auth.sms.send(value, function (error, value) {
                    loginPage.locked = false;
                    if (error) {
                        app.controls.box('alert', null, 'پیامک ارسال نشد.', 'center');
                        console.error('Failed to send sms');
                    }
                    else
                        verifyPage.show();
                });

            }
        } )
    });

}, function () {
    // before page shown

});